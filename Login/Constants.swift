//
//  Constants.swift
//  Login
//
//  Created by Javier Ramon Gil on 19/5/15.
//  Copyright (c) 2015 Javier Ramon. All rights reserved.
//

import Foundation

let versionKey = "version"
let autoSesionKey = "autoSesion"
let userKey = "user"
let passKey = "pass"