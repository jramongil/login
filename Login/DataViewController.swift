//
//  DataViewController.swift
//  Login
//
//  Created by Javier Ramon Gil on 19/5/15.
//  Copyright (c) 2015 Javier Ramon. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {
    @IBOutlet var userDataLabel: UILabel!
    @IBOutlet var passDataLabel: UILabel!
    @IBOutlet var buttonExit: UIButton!
    var user:String!
    var pass:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = NSUserDefaults.standardUserDefaults()
        

        // Do any additional setup after loading the view.
        buttonExit.layer.borderWidth = 2
        buttonExit.layer.borderColor = UIColor.grayColor().CGColor
        
        refreshFields()

        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let app = UIApplication.sharedApplication()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationWillEnterForeground:", name: UIApplicationWillEnterForegroundNotification, object: app)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        refreshFields()
    }
    
    
    func applicationWillEnterForeground(notification:NSNotification) {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if defaults.boolForKey(autoSesionKey) == true {
            let dataVC = storyboard?.instantiateViewControllerWithIdentifier("dataViewController") as! DataViewController
            dataVC.user = defaults.stringForKey(userKey)
            dataVC.pass = defaults.stringForKey(passKey)
            presentViewController(dataVC, animated: false, completion: nil)
        }else {
            let loginVC = storyboard?.instantiateViewControllerWithIdentifier("loginViewController") as! LoginViewController
            presentViewController(loginVC, animated: false, completion: nil)

        }
    }
    
    
    @IBAction func buttonExitPressed(sender: AnyObject) {
        let loginVC = storyboard?.instantiateViewControllerWithIdentifier("loginViewController") as! LoginViewController
        loginVC.user = userDataLabel.text
        loginVC.pass = passDataLabel.text
        presentViewController(loginVC, animated: false, completion: nil)
    }
    
    func refreshFields(){
        userDataLabel.text = user
        passDataLabel.text = pass
    }
 
}
