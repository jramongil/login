//
//  LoginViewController.swift
//  Login
//
//  Created by Javier Ramon Gil on 19/5/15.
//  Copyright (c) 2015 Javier Ramon. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet var userLogin: UITextField!
    @IBOutlet var passLogin: UITextField!
    @IBOutlet var buttonLogin: UIButton!
    var user:String!
    var pass:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = NSUserDefaults.standardUserDefaults()
       

        // Do any additional setup after loading the view.
        buttonLogin.layer.borderWidth = 2
        buttonLogin.layer.borderColor = UIColor.grayColor().CGColor

        if defaults.boolForKey(autoSesionKey) == false {
            userLogin.text = user
            passLogin.text = pass
        }
        
        
        
        
    }
        
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let defaults = NSUserDefaults.standardUserDefaults()

        let app = UIApplication.sharedApplication()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationWillEnterForeground:", name: UIApplicationWillEnterForegroundNotification, object: app)
    }
    
    
    func applicationWillEnterForeground(notification:NSNotification) {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if defaults.boolForKey(autoSesionKey) == true {
            let dataVC = storyboard?.instantiateViewControllerWithIdentifier("dataViewController") as! DataViewController
            dataVC.user = defaults.stringForKey(userKey)
            dataVC.pass = defaults.stringForKey(passKey)
            presentViewController(dataVC, animated: false, completion: nil)
        }
    }
    
    @IBAction func buttonLoginPressed(sender: AnyObject) {
        let dataVC = storyboard?.instantiateViewControllerWithIdentifier("dataViewController") as! DataViewController
        dataVC.user = userLogin.text
        dataVC.pass = passLogin.text
        presentViewController(dataVC, animated: false, completion: nil)
    }
    


}
